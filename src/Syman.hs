
{-

Evaluation (and symbolic manipulation (TODO)) of logic expressions in Haskell.

-}

{-# LANGUAGE FlexibleInstances #-}

module Syman where

import qualified Data.Map as Map
import Data.List
import Data.Char
import Parsing

implies :: Bool -> Bool -> Bool
implies x y = not x || y

iff :: Bool -> Bool -> Bool
iff x y = implies x y && implies y x

xor :: Bool -> Bool -> Bool
xor x y = not (iff x y)

nand :: Bool -> Bool -> Bool
nand x y = not (x && y)

nor :: Bool -> Bool -> Bool
nor x y = not (x || y)

--We will mostly be dealing with expressions of type 'Exp String String', but
--might as well be as flexible as possible.

data Exp n b = N n
             | U (Exp n b)
             | B b (Exp n b) (Exp n b)
             deriving (Eq)

mapExp :: (n -> m) -> (b -> a) -> Exp n b -> Exp m a
mapExp fn fb = g where
  g (N v) = N (fn v)
  g (U e) = U (g e)
  g (B op e1 e2) = B (fb op) (g e1) (g e2)

foldExp :: (n -> c) -> (c -> c) -> (b -> c -> c -> c) -> Exp n b -> c
foldExp fn fu fb = g where
  g (N v) = fn v
  g (U e) = fu (g e)
  g (B op e1 e2) = fb op (g e1) (g e2)

--evaluate :: Exp Bool (Bool -> Bool -> Bool) -> Bool
--evaluate = foldExp id not (\op e1 e2 -> e1 `op` e2)

--The above works for expressions contains functions and bools directly, eg:
--evaluate $ B (&&) (U (N False)) (N True) ==> True

instance Show (Exp String String) where
  show exp = (recurse False exp) "" where
    recurse hasParent (N v) = showString v
    recurse hasParent (U e) =
      showNot . showParen (not (nullary e) && hasParent) (recurse True e)
    recurse hasParent (B op e1 e2) =
      showParen hasParent (recurse True e1 . showSpace . showString op .
                           showSpace . recurse True e2)
    nullary (N v) = True
    nullary _ = False
    showNot = showChar '~'
    showSpace = showChar ' '

allVars :: Exp String String -> [String]
allVars = foldExp (: []) id (\op e1 e2 -> e1 `union` e2)

lookupVal :: Ord k => Map.Map k v -> k -> v
lookupVal table key =
  case (Map.lookup key table) of
    Just x -> x
    Nothing -> error "Entry not found."

nullaryOperators :: Map.Map String Bool
nullaryOperators = Map.fromList [("T", True), ("F", False)]

binaryOperators :: Map.Map String (Bool -> Bool -> Bool)
binaryOperators =
  Map.fromList [("and", (&&)), ("or", (||)), ("implies", implies), ("iff", iff),
                ("xor", xor), ("nand", nand), ("nor", nor)]

insertFromList :: Ord k => [(k, v)] -> Map.Map k v -> Map.Map k v
insertFromList [] table = table
insertFromList ((key, val):xs) table =
  insertFromList xs (Map.insert key val table)

--Must pass (k, v) bindings for any propositional variables used.
evalWhere :: [(String, Bool)] -> Exp String String -> Bool
evalWhere vars exp = evaluate exp where
  varTable = insertFromList vars nullaryOperators
  opTable = binaryOperators
  evaluate =
    foldExp (lookupVal varTable) not (\op e1 e2 ->
                                        (lookupVal opTable op) e1 e2)

equivWhere :: [(String, Bool)] -> Exp String String-> Exp String String-> Bool
equivWhere xs exp1 exp2 = (evalWhere xs exp1) == (evalWhere xs exp2)

--Adapted from: https://wiki.haskell.org/99_questions/Solutions/49
grayCode :: Int -> [[Bool]]
grayCode 0 = [[]]
grayCode n = map (False :) xs ++ map (True :) (reverse xs) where
  xs = grayCode (n-1)

--Gray code order
allInterpretations :: [String] -> [[(String, Bool)]]
allInterpretations [] = [[]]
allInterpretations xs = map (zip xs) (grayCode (length xs))

--See: https://stackoverflow.com/questions/957642/
--     generating-truth-tables-for-logic-expressions-in-haskell

--Default order
--allInterpretations :: [String] -> [[(String, Bool)]]
--allInterpretations [] = [[]]
--allInterpretations (x:xs) =
--  [(x,y) : r | y <- [False, True], r <- allInterpretations xs]

data TruthTable = Table [[(String, Bool)]]

columns :: TruthTable -> [String]
columns (Table [[]]) = []
columns (Table rows) = map fst (head rows)

values :: TruthTable -> [[Bool]]
values (Table [[]]) = []
values (Table rows) = map (map snd) rows

align :: Int -> [String] -> String
align n [] = []
align n (x:xs) = x ++ (replicate (n - (length x)) ' ') ++ align n xs

instance Show TruthTable where
  show table =
    align 10 (columns table) ++
    foldr (++) [] (map (("\n" ++) . (align 10)) (map (map show) (values table)))

truthTable' :: Exp String String -> [[(String, Bool)]]
truthTable' exp = map f (allInterpretations (allVars exp)) where
  f :: [(String, Bool)] -> [(String, Bool)]
  f pairs = pairs ++ [((show exp), (evalWhere pairs exp))]

truthTable :: Exp String String -> TruthTable
truthTable = Table . truthTable'

equiv :: Exp String String -> Exp String String -> Bool
equiv exp1 exp2 =
  and (zipWith (==)
       (map ((flip evalWhere) exp1) vals)
       (map ((flip evalWhere) exp2) vals)) where
  vals = allInterpretations (union (allVars exp1) (allVars exp2))

--Doesn't check if expressions are actually minterms
orMinterms :: [Exp String String] -> Exp String String
orMinterms = foldr1 (B "or")

--Vice versa
andMaxterms :: [Exp String String] -> Exp String String
andMaxterms = foldr1 (B "and")

--------------------------------------------------------------------------------
-- Parser for fully-parenthesized expressions
identifier' :: Parser (Exp String String)
identifier' = token f where
  f = do {x <- some (sat isAlpha); return (N x)}

operator' :: Parser String
operator' = foldr1 (<|>) (map f ops) where
  ops = ["and", "or", "xor", "implies", "iff", "nand", "nor"]
  f x = symbol x >> return x

nullary' :: Parser (Exp String String)
nullary' = identifier'

unary' :: Parser (Exp String String)
unary' = token f where
  f = do {char '~';
          exp1 <- expr';
          return (U exp1)}

binary' :: Parser (Exp String String)
binary' = token f where
  f = do {exp1 <- expr';
          op <- operator';
          exp2 <- expr';
          return (B op exp1 exp2)}

expr' :: Parser (Exp String String)
expr' = token (nullary' <|> unary' <|> paren binary')

parseExp :: String -> Exp String String
parseExp s = case (apply expr' s) of
  [] -> error "Invalid input. Must fully parenthesize all binary expressions."
  [(exp1, _)] -> exp1

--------------------------------------------------------------------------------
--Examples (update these and define tests using Test.QuickCheck and/or HUnit!!!)

{-
foo = (U "not" (B "and" (B "implies" (N "a") (N "b")) (N "c")))

dnf1 = orMinterms [(B "and" (U "not" (N "a")) (U "not" (N "c"))),
                   (B "and" (N "a") (U "not" (N "b"))),
                   (B "and" (N "a") (U "not" (N "c"))),
                   (B "and" (N "b") (U "not" (N "c")))]

Examples:

evalWhere [("p", True), ("q", False)] (B "and" (N "p") (N "q"))
  => False
foo = (U "not" (B "and" (B "implies" (N "a") (N "b")) (N "c")))
equiv foo (B "implies" (N "c") (B "and" (N "a") (U "not" (N "b"))))
  => True
equiv foo (B "implies" (B "and" (N "b") (N "c")) (N "a"))
  => False
equiv foo dnf1
  => True

parseExp (show foo)
  => ~((a implies b) and c)
parseExp (show foo) == foo
  => True
parseExp (show dnf1) == dnf1
  => False
parseExp ("(" ++ show dnf1 ++ ")") == dnf1
  => True

parseExp "~(p)"
  => error

orMinterms $ map parseExp ["(p and ~q)", "(~p and r)", "(q and r)"]
  => (p and ~q) or ((~p and r) or (q and r))

How to do K-Maps?

Allow "don't care" values? Would need to create my own 3-way Bool type and
modify everything else accordingly. Could use list comprehensions to make
truth tables structures with all values filled in True or False, and then
selectively edit the ones I don't care about?

-}
