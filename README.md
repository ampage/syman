
# Syman

Symbolic manipulation and evaluation of Boolean expressions in Haskell.

NOTE: the original point of this project was to write a Karnaugh-map solver,
but I got side-tracked by parser combinators, which I learned about the following
year at university. I haven't revisited this since due to lack of time.

