
{-# LANGUAGE FlexibleInstances #-}

import Test.Tasty (defaultMain, testGroup, TestTree)
import Test.Tasty.HUnit (assertEqual, assertBool, testCase)
import Test.Tasty.SmallCheck as SC
import Test.Tasty.QuickCheck as QC
import Control.Monad

import Syman

main :: IO ()
--main = putStrLn "Insert tests here!"
main = defaultMain unitTests

tests :: TestTree
tests = testGroup "Tests" [unitTests] --properties

--properties :: TestTree
--properties = testGroup "Properties" [scProps, qcProps]

foo = (U (B "and" (B "implies" (N "a") (N "b")) (N "c")))

unitTests :: TestTree
unitTests = testGroup "Unit tests" [fooParse]

fooParse =
  testCase "parseExp (show foo) == foo" $ assertBool "Expressions differ." (prop_parser foo)

prop_parser exp = parseExp (show exp) == exp
